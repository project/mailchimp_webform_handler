Mailchimp Webform Handler
===========
Mailchimp Webform Handler provides a new Webform Handler plugin to send submission data to Mailchimp via their API.


Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8


Configuration
------------

Add a new handler of type "MailChimp Webform Handler" to the webform.
Add your Mailchimp API-key and server prefix.
Click the button to update the Lists. Select your list and map your Mailchimp-fields to your webform fields.